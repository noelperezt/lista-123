from django.shortcuts import render, redirect, render_to_response
from apps.solicitudes.forms import SolicitudForm
from apps.sugerencias.forms import SugerenciaForm
from apps.grados.models import Grado
from apps.solicitudes.models import Solicitud
from django.core.mail import EmailMessage
from django.template import Context
from django.template.loader import get_template
from django.db import connection
from django.contrib import messages

# Create your views here.
def index_principal(request):
    solicitud_form = SolicitudForm(data=request.POST)
    sugerencia_form = SugerenciaForm(data=request.POST)
    grado = Grado.objects.all()
    if request.method == "POST":
        if "change_sugerencia" in request.POST and sugerencia_form.is_valid():
            sugerencia_form.save()
            messages.add_message(request, messages.SUCCESS, 'ok();')
            return redirect('inicio')
        

        if "change_solicitud" in request.POST and solicitud_form.is_valid():
            data = solicitud_form.cleaned_data
            ctx = {
            }
            message = get_template('mail/Bienvenido.html').render(Context(ctx))
            msg = EmailMessage('Solicitud de Cotizacion', message, 'hola@admin.com', [data['correo']])
            msg.content_subtype = 'html'
            msg.send()
            solicitud_form.save()
            return redirect('inicio')

    else:
        solicitud_form = SolicitudForm()
        sugerencia_form = SugerenciaForm()
    return render(request, 'landing/landing.html', {'form': solicitud_form, 'form1':sugerencia_form, 'grados':grado,})

def index(request):
    solicitud = Solicitud.objects.exclude(estado='E').order_by('id')
    contexto = {'solicitudes':solicitud}
    pagi
    return render(request,'solicitudes/listar.html', contexto)

def cotizar(request, id_solicitud):
    query = "SELECT productos_producto.nombre as NOMBRE, productos_listaprecio.precio\
             FROM productos_producto, productos_listaprecio, listas_lista_producto, listas_lista, institutos_instituto\
             WHERE productos_listaprecio.producto_id = productos_producto.id\
             AND productos_producto.id = listas_lista_producto.producto_id\
             AND listas_lista_producto.lista_id = listas_lista.id\
             AND listas_lista.instituto_id = institutos_instituto.id\
             AND institutos_instituto.categoria_id = productos_listaprecio.categoria_id\
             AND listas_lista.id = (SELECT listas_lista.id FROM listas_lista, solicitudes_solicitud\
             WHERE listas_lista.instituto_id = solicitudes_solicitud.instituto_id\
             AND listas_lista.grado_id = solicitudes_solicitud.grado_id\
             AND solicitudes_solicitud.id = "+ id_solicitud +")"

    with connection.cursor() as cursor:
        cursor.execute(query)
        rows = cursor.fetchall()

        contexto = Solicitud.objects.filter(id = id_solicitud)

        if contexto:
            for p in contexto:
                correo = p.correo
                estudiante = p.estudiante

        total = 0
        for a in rows:
            total = total + a[1]

        ctx = {
            'consulta':rows,
            'estudiante':estudiante,
            'precio':total,
        }
        message = get_template('mail/Cotizacion.html').render(Context(ctx))
        msg = EmailMessage('Solicitud de Cotizacion', message, 'hola@admin.com', [correo])
        msg.content_subtype = 'html'
        msg.send()

        query ="UPDATE solicitudes_solicitud SET estado = 'PR' WHERE id ="+id_solicitud
        cursor.execute(query)

    return redirect('solicitudes:index')

def solicitudes_delete (request, id_solicitud):
    solicitud = Solicitud.objects.get(id = id_solicitud)
    if request.method == 'POST':
        solicitud.delete()
        return redirect('solicitudes:index')
    return render(request, 'solicitudes/eliminar.html', {'solicitud':solicitud})

def despachar (request, id_solicitud):
    solicitud = Solicitud.objects.get(id = id_solicitud)
    if request.method == 'POST':
        query = "UPDATE solicitudes_solicitud SET estado = 'E' WHERE id =" + id_solicitud
        with connection.cursor() as cursor:
            cursor.execute(query)
        return redirect('solicitudes:index')
    return render(request, 'solicitudes/enviar.html', {'solicitud': solicitud})
