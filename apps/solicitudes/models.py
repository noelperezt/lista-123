from django.db import models
from apps.institutos.models import Instituto
from apps.grados.models import Grado
from apps.productos.models import Producto

# Create your models here.
class Solicitud(models.Model):
    representante = models.CharField(max_length=60)
    telefono = models.CharField(max_length=10)
    correo = models.EmailField()
    instituto = models.ForeignKey(Instituto, null=False, blank=False, on_delete=models.CASCADE)
    grado = models.ForeignKey(Grado, null=False, blank=False, on_delete=models.CASCADE)
    estudiante = models.CharField(max_length=70)
    direccion = models.CharField(max_length=100)
    estado = models.CharField(max_length=2)


