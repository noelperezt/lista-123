from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from apps.solicitudes.views import index, cotizar, solicitudes_delete, despachar

urlpatterns = [
    url(r'^$', login_required(index), name='index'),
    url(r'^cotizar/(?P<id_solicitud>\d+)/$', login_required(cotizar), name='cotizacion'),
    url(r'^eliminar/(?P<id_solicitud>\d+)/$', login_required(solicitudes_delete), name='eliminar'),
    url(r'^enviar/(?P<id_solicitud>\d+)/$', login_required(despachar), name='enviar'),
]