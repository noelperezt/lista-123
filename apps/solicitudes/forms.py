from django import forms
from apps.solicitudes.models import Solicitud

class SolicitudForm(forms.ModelForm):

    class Meta:
        model = Solicitud

        fields =[
            'representante',
            'telefono',
            'correo',
            'instituto',
            'grado',
            'estudiante',
            'direccion',
            'estado',
        ]

        labels = {
            'representante': 'Representante',
            'telefono': 'Teléfono',
            'correo': 'Correo',
            'instituto': 'Instituto',
            'grado': 'Grado',
            'estudiante': 'Estudiante',
            'direccion':'Direccion',
            'estado': 'Estado',
        }

        widgets = {
            'representante': forms.TextInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Representante'}),
            'telefono': forms.NumberInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Teléfono'}),
            'correo': forms.EmailInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Correo'}),
            'instituto': forms.HiddenInput(attrs={'class':'form-control border-input'}),
            'grado': forms.Select(attrs={'class':'form-control border-input'}),
            'estudiante': forms.TextInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Estudiante'}),
            'direccion': forms.Textarea(attrs={'class': 'form-control border-input', 'type': 'text', 'placeholder': 'Direccion'}),
            'estado': forms.HiddenInput(attrs={'class': 'form-control border-input', 'type': 'text', 'value': 'P'})
        }