from django.db import models

# Create your models here.
class Grado(models.Model):
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return '{}'.format(self.descripcion)