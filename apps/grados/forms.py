from django import forms
from apps.grados.models import Grado

class GradosForm(forms.ModelForm):

    class Meta:
        model = Grado

        fields =[
            'descripcion',
        ]

        labels = {
            'descripcion': 'Descripción',
        }

        widgets = {
            'descripcion': forms.TextInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Grado',}),
        }

