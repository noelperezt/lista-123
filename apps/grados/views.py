from django.shortcuts import render, redirect
from django.http import HttpResponse
from apps.grados.forms import GradosForm
from apps.grados.models import Grado

# Create your views here.
def index(request):
    grado = Grado.objects.all().order_by('id')
    contexto = {'grados':grado}
    return render(request,'grados/listar.html', contexto)

def grados_view (request):
    if request.method == 'POST':
        form = GradosForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('grados:index')
    else:
        form = GradosForm()
    return render(request, 'grados/agregar.html', {'form': form})

def grados_edit (request, id_grado):
    grado = Grado.objects.get(id = id_grado)
    if request.method == 'GET':
        form = GradosForm(instance=grado)
    else:
        form = GradosForm(request.POST, instance=grado)
        if form.is_valid():
            form.save()
        return redirect('grados:index')
    return render(request, 'grados/editar.html', {'form':form})

def grados_delete (request, id_grado):
    grado = Grado.objects.get(id = id_grado)
    if request.method == 'POST':
        grado.delete()
        return redirect('grados:index')
    return render(request, 'grados/eliminar.html', {'grado':grado})
