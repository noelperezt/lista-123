from django.contrib import admin
from apps.grados.models import Grado

# Register your models here.
admin.site.register(Grado)
