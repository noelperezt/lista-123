from django.conf.urls import url
from apps.grados.views import index, grados_view, grados_edit, grados_delete
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', login_required(index), name='index'),
    url(r'^nuevo$', login_required(grados_view), name='grados_crear'),
    url(r'^editar/(?P<id_grado>\d+)/$', login_required(grados_edit), name='grados_editar'),
    url(r'^eliminar/(?P<id_grado>\d+)/$', login_required(grados_delete), name='grados_eliminar'),
]