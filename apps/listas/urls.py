from django.conf.urls import url
from apps.listas.views import index,listas_view, listas_edit, listas_delete, listas_detalle
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', login_required(index), name='index'),
    url(r'^nuevo$',login_required(listas_view) , name='listas_crear'),
    url(r'^editar/(?P<id_lista>\d+)/$', login_required(listas_edit), name='listas_editar'),
    url(r'^eliminar/(?P<id_lista>\d+)/$', login_required(listas_delete), name='listas_eliminar'),
    url(r'^detalle/(?P<id_lista>\d+)/$', login_required(listas_detalle), name='listas_detalle'),
]
