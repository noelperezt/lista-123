from django import forms
from apps.listas.models import Lista

class ListasForm (forms.ModelForm):

    class Meta:
        model = Lista

        fields = [
            'instituto',
            'grado',
            'producto',
        ]

        labels = {
            'instituto': 'Instituto',
            'grado': 'Grado',
            'producto' : 'Producto',
        }

        widgets = {
            'instituto' : forms.HiddenInput(attrs={'class':'form-control border-input'}),
            'grado' : forms.Select(attrs={'class':'form-control border-input'}),
            'producto' : forms.SelectMultiple(attrs={'class':'form-control border-input'}),
        }
