from django.shortcuts import render, redirect
from apps.listas.models import Lista
from apps.institutos.models import Instituto
from apps.utilidades.models import Departamento, Municipio
from apps.productos.models import Producto
from apps.listas.forms import ListasForm

# Create your views here.
def index(request):
    #lista = Lista.objects.all().order_by('id')
    lista = Lista.objects.select_related("instituto").select_related("grado").order_by('instituto__nombre')
    contexto = {'listas':lista}
    return render(request,'listas/listar.html', contexto)

def listas_view (request):
    if request.method == 'POST':
        form = ListasForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('listas:index')
    else:
        form = ListasForm()
    return render(request, 'listas/agregar.html', {'form': form})

def listas_edit (request, id_lista):
    lista = Lista.objects.get(id = id_lista)
    departamentos = Departamento.objects.all()
    municipios = Municipio.objects.filter(departamento=lista.instituto.departamento.pk)
    institutos = Instituto.objects.filter(municipio=lista.instituto.municipio.pk)
    if request.method == 'GET':
        form = ListasForm(instance=lista)
    else:
        form = ListasForm(request.POST, instance=lista)
        if form.is_valid():
            form.save()
        return redirect('listas:index')
    return render(request, 'listas/editar.html', {'form':form, 'departamentos':departamentos, 'departamento': lista.instituto.departamento_id, 'municipios': municipios, 'municipio': lista.instituto.municipio_id, 'institutos': institutos, 'instituto': lista.instituto.pk})

def listas_delete (request, id_lista):
    lista = Lista.objects.get(id = id_lista)
    if request.method == 'POST':
        lista.delete()
        return redirect('listas:index')
    return render(request, 'listas/eliminar.html', {'lista':lista})

def listas_detalle(request, id_lista):
    producto = Producto.objects.filter(lista=id_lista)
    contexto = {'productos': producto}
    return render(request,'listas/detalle.html', contexto)
