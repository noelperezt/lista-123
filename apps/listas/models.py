from django.db import models
from apps.institutos.models import Instituto
from apps.grados.models import Grado
from apps.productos.models import Producto

# Create your models here.
class Lista(models.Model):
    instituto = models.ForeignKey(Instituto, null=False, blank=False, on_delete=models.CASCADE)
    grado = models.ForeignKey(Grado, null =False, blank=False, on_delete=models.CASCADE)
    producto = models.ManyToManyField(Producto)