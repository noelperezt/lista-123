from django import forms
from apps.sugerencias.models import Sugerencia


class SugerenciaForm(forms.ModelForm):

    class Meta:
        model = Sugerencia

        fields =[
            'descripcion',
        ]

        labels = {
            'descripcion':'Descripcion',
        }

        widgets = {
            'descripcion': forms.Textarea(attrs={ 'type': 'text', 'placeholder': 'Escriba su Sugerencia'}),
        }
