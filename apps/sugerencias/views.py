from django.shortcuts import render, redirect
from apps.sugerencias.models import Sugerencia
from apps.sugerencias.forms import SugerenciaForm

# Create your views here.
def index(request):
    sugerencia = Sugerencia.objects.all().order_by('id')
    contexto = {'sugerencias':sugerencia}
    return render(request,'sugerencias/listar.html', contexto)


def sugerencia_delete (request, id_sugerencia):
    sugerencia = Sugerencia.objects.get(id = id_sugerencia)
    if request.method == 'POST':
        sugerencia.delete()
        return redirect('sugerencias:index')
    return render(request, 'sugerencias/eliminar.html', {'sugerencia':sugerencia})