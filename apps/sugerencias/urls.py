from django.conf.urls import url
from apps.sugerencias.views import index, sugerencia_delete
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', login_required(index), name='index'),
    url(r'^eliminar/(?P<id_sugerencia>\d+)/$', login_required(sugerencia_delete), name='sugerencias_eliminar'),
]