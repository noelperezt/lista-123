from django.shortcuts import render, redirect
from apps.productos.forms import ProductosForm, PreciosForm
from apps.productos.models import Producto, ListaPrecio, Categoria

# Create your views here.
def index(request):
    producto = Producto.objects.all().order_by('id')
    contexto = {'productos':producto}
    return render(request,'productos/listar.html', contexto)

def productos_view(request):
    if request.method == 'POST':
        form = ProductosForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('productos:index')
    else:
        form = ProductosForm()
    return render(request, 'productos/agregar.html', {'form': form})

def productos_edit (request, id_producto):
    producto = Producto.objects.get(id=id_producto)
    if request.method == 'GET':
        form = ProductosForm(instance=producto)
    else:
        form = ProductosForm(request.POST, instance=producto)
        if form.is_valid():
            form.save()
        return redirect('productos:index')
    return render(request, 'productos/editar.html', {'form':form})

def productos_delete (request, id_producto):
    producto = Producto.objects.get(id = id_producto)
    if request.method == 'POST':
        producto.delete()
        return redirect('productos:index')
    return render(request, 'productos/eliminar.html', {'producto':producto})

def productos_precio(request, id_producto):
    #producto = Producto.objects.filter(precio__producto= id_producto)
    producto = ListaPrecio.objects.select_related("categoria").filter(producto=id_producto)
    contexto = {'productos':producto, 'id_producto' : id_producto}
    return render(request,'productos/listaprecios.html', contexto)

def productos_precio_nuevo(request, id_producto):
    if request.method == 'POST':
        form = PreciosForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('productos:productos_precio', id_producto = id_producto)
    else:
        form = PreciosForm
    return render(request, 'productos/agregarprecio.html', {'form': form, 'producto':id_producto })

def productos_precio_edit (request, id_producto, id_precio):
    precio = ListaPrecio.objects.get(id=id_precio)
    if request.method == 'GET':
        form = PreciosForm(instance=precio)
    else:
        form = PreciosForm(request.POST, instance=precio)
        if form.is_valid():
            form.save()
        return redirect('productos:productos_precio', id_producto=id_producto)
    return render(request, 'productos/editarprecio.html', {'form':form, 'producto':id_producto})

def productos_precio_delete (request, id_producto, id_precio):
    precio = ListaPrecio.objects.get(id=id_precio)
    if request.method == 'POST':
        precio.delete()
        return redirect('productos:productos_precio', id_producto=id_producto)
    return render(request, 'productos/eliminarprecio.html', {'precio':precio, 'producto': id_producto})