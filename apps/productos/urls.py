from django.conf.urls import url
from apps.productos.views import index, productos_view, productos_edit, productos_delete, productos_precio, productos_precio_nuevo, productos_precio_edit, productos_precio_delete
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', login_required(index), name='index'),
    url(r'^nuevo$', login_required(productos_view), name='productos_crear'),
    url(r'^editar/(?P<id_producto>\d+)/$', login_required(productos_edit), name='productos_editar'),
    url(r'^eliminar/(?P<id_producto>\d+)/$', login_required(productos_delete), name='productos_eliminar'),
    url(r'^precio/(?P<id_producto>\d+)/$', login_required(productos_precio), name='productos_precio'),
    url(r'^precio/nuevo/(?P<id_producto>\d+)/$', login_required(productos_precio_nuevo), name='productos_precio_nuevo'),
    url(r'^precio/editar/(?P<id_producto>\d+)/(?P<id_precio>\d+)/$', login_required(productos_precio_edit), name='productos_precio_editar'),
    url(r'^precio/eliminar/(?P<id_producto>\d+)/(?P<id_precio>\d+)/$', login_required(productos_precio_delete), name='productos_precio_eliminar')
]