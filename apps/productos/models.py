from django.db import models
from apps.utilidades.models import Categoria

# Create your models here.
class Producto(models.Model):
    nombre = models.TextField()
    fecha_registro = models.DateField()
    garantia = models.BooleanField()
    codigo = models.CharField(max_length=50)
    precio = models.ManyToManyField(Categoria, through= 'ListaPrecio', through_fields= ('producto', 'categoria'))

    def __str__(self):
        return '{}'.format(self.nombre)

class ListaPrecio(models.Model):
    producto = models.ForeignKey(Producto, null=False, blank=False)
    categoria = models.ForeignKey(Categoria, null=False, blank=False)
    precio = models.DecimalField(max_digits=20, decimal_places=2)
