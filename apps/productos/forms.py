from django import forms
from apps.productos.models import Producto, ListaPrecio


class ProductosForm(forms.ModelForm):

    class Meta:
        model = Producto

        fields =[
            'nombre',
            'fecha_registro',
            'garantia',
            'codigo',
        ]

        labels = {
            'nombre':'Nombre',
            'fecha_registro' :'Fecha',
            'garantia' :'Garantia',
            'codigo':'Codigo',
        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Nombre'}),
            'fecha_registro': forms.DateInput(attrs={'id': 'datepicker','class':'form-control border-input', 'type': 'text', 'placeholder': 'dd/MM/YYYY'}),
            'garantia': forms.CheckboxInput(attrs={'class':'form-control border-input',}),
            'codigo': forms.NumberInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Código'}),
        }

class PreciosForm(forms.ModelForm):

    class Meta:
        model = ListaPrecio

        fields = [
            'producto',
            'categoria',
            'precio',
        ]

        labels = {
            'producto': 'Producto',
            'categoria': 'Categoria',
            'precio': 'Precio',
        }

        widgets = {
            'producto' :forms.HiddenInput(attrs={'class':'form-control border-input', 'id' : 'producto'}),
            'categoria' : forms.Select(attrs={'class':'form-control border-input'}),
            'precio': forms.NumberInput(attrs={'class':'form-control border-input'}),
        }