from django.contrib import admin
from apps.utilidades.models import *

# Register your models here.
admin.site.register(Departamento)
admin.site.register(Municipio)
admin.site.register(Categoria)