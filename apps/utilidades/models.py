from django.db import models
from smart_selects.db_fields import ChainedForeignKey

# Create your models here.
class Departamento(models.Model):
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return '{}'.format(self.descripcion)

class Municipio(models.Model):
    descripcion = models.CharField(max_length=50)
    departamento = models.ForeignKey(Departamento, null=False, blank=False)

    def __str__(self):
        return '{}'.format(self.descripcion)

class Categoria(models.Model):
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return '{}'.format(self.descripcion)