from django.shortcuts import render,redirect
from apps.institutos.forms import InstitutosForm
from apps.institutos.models import Instituto, Municipio, Departamento
from django.http import HttpResponseBadRequest, HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    instituto = Instituto.objects.all().order_by('id')
    contexto = {'institutos':instituto}
    return render(request,'institutos/listar.html', contexto)

def institutos_view (request):
    if request.method == 'POST':
        form = InstitutosForm(request.POST)
        if form.is_valid():
            instituto = form.save(commit=False)
            instituto.municipio_id = request.POST.get('id_municipio')
            instituto.save()
        return redirect('institutos:index')
    else:
        form = InstitutosForm()
    return render(request, 'institutos/agregar.html', {'form': form})

def institutos_edit (request, id_instituto):
    instituto = Instituto.objects.get(id = id_instituto)
    municipios = Municipio.objects.filter(departamento_id = instituto.departamento)
    if request.method == 'GET':
        form = InstitutosForm(instance=instituto)
    else:
        form = InstitutosForm(request.POST, instance=instituto)
        if form.is_valid():
            instituto = form.save(commit=False)
            instituto.municipio_id = request.POST.get('id_municipio')
            instituto.save()
        return redirect('institutos:index')
    return render(request, 'institutos/editar.html', {'form':form,'municipios': municipios,'municipio':instituto.municipio })

def institutos_delete (request, id_instituto):
    instituto = Instituto.objects.get(id = id_instituto)
    if request.method == 'POST':
        instituto.delete()
        return redirect('institutos:index')
    return render(request, 'institutos/eliminar.html', {'instituto':instituto})

def get_municipio(request):
    """Obtiene una lista de categorias dado un padre"""
    # si no es una peticion ajax, devolvemos error 400
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    # definimos el termino de busqueda
    padre = request.POST['departamento']
    hijos = Municipio.objects.filter(departamento_id=padre)

    prod_fields = ('id', 'descripcion', )

    data = serializers.serialize('json', hijos, fields=prod_fields)
    return HttpResponse(data, content_type="application/json")

def get_departamentos(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    padre = Departamento.objects.all()

    prod_fields = ('id', 'descripcion',)

    data = serializers.serialize('json',padre,fields= prod_fields)
    return HttpResponse(data, content_type="application/json")

def get_instituto(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

        # definimos el termino de busqueda
    padre = request.POST['municipio']
    hijos = Instituto.objects.filter(municipio_id=padre)

    prod_fields = ('id', 'nombre',)

    data = serializers.serialize('json', hijos, fields=prod_fields)
    return HttpResponse(data, content_type="application/json")
