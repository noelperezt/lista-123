from django import forms
from apps.institutos.models import Instituto,Departamento
from smart_selects.form_fields import ModelChoiceField

class InstitutosForm(forms.ModelForm):
    '''nombre = forms.TextInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Nombre',})
    nombre.render('nombre',None)
    carrera = forms.TextInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Carrera',})
    calle = forms.Textarea(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Direccion',})
    municipio = forms.Select(attrs={'class': 'form-control border-input'})
    departamento = forms.Select(attrs={'class': 'form-control border-input'})
    categoria = forms.Select(attrs={'class': 'form-control border-input'})'''

    class Meta:
        model = Instituto

        fields =[
            'nombre',
            'carrera',
            'calle',
            'departamento',
            'categoria',
        ]

        labels = {
            'nombre': 'Nombre',
            'carrera': 'Carrera',
            'calle': 'Calle',
            'departamento': 'Departamento',
            'categoria' : 'Categoria',
        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Nombre',}),
            'carrera': forms.TextInput(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Carrera',}),
            'calle': forms.Textarea(attrs={'class':'form-control border-input', 'type': 'text', 'placeholder': 'Direccion',}),
            'departamento': forms.Select(attrs={'class':'form-control border-input'}),
            'categoria': forms.Select(attrs={'class':'form-control border-input'}),
        }