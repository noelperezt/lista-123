from django.conf.urls import url
from apps.institutos.views import index,institutos_view,institutos_edit,institutos_delete, get_municipio, get_departamentos, get_instituto
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', login_required(index), name='index'),
    url(r'^nuevo$', login_required(institutos_view), name='institutos_crear'),
    url(r'^editar/(?P<id_instituto>\d+)/$', login_required(institutos_edit), name='institutos_editar'),
    url(r'^eliminar/(?P<id_instituto>\d+)/$', login_required(institutos_delete), name='institutos_eliminar'),
    url(r'^buscar_departamento$', login_required(get_departamentos), name='buscar_departamento'),
    url(r'^buscar_municipio$', login_required(get_municipio), name='buscar_municipio'),
    url(r'^buscar_instituto$', login_required(get_instituto), name='buscar_instituto'),
]