from django.db import models
from apps.utilidades.models import Categoria,Municipio,Departamento

# Create your models here.
class Instituto(models.Model):
    nombre = models.CharField(max_length=50)
    carrera = models.CharField(max_length=50)
    calle = models.CharField(max_length=60)
    departamento = models.ForeignKey(Departamento, null=False, blank=False)
    municipio = models.ForeignKey(Municipio, null=False, blank=False)
    categoria = models.ForeignKey(Categoria, null=False, blank=False)

    def __str__(self):
        return '{}'.format(self.nombre)