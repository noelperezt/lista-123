"""lista123 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from apps.solicitudes.views import index_principal
from django.contrib.auth.views import login, logout_then_login
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', index_principal, name='inicio'),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/login/$', login,{'template_name':'base/login.html'} ,name='login'),
    url(r'^logout/$', logout_then_login, name='logout'),
    url(r'^dashboard/', include('apps.dashboard.urls', namespace="dashboard")),
    url(r'^dashboard/grados/', include('apps.grados.urls', namespace="grados")),
    url(r'^dashboard/institutos/', include('apps.institutos.urls', namespace="institutos")),
    url(r'^dashboard/listas/', include('apps.listas.urls', namespace="listas")),
    url(r'^dashboard/productos/', include('apps.productos.urls', namespace="productos")),
    url(r'^dashboard/solicitudes/', include('apps.solicitudes.urls', namespace="solicitudes")),
    url(r'^dashboard/sugerencias/', include('apps.sugerencias.urls', namespace="sugerencias")),
    url(r'^dashboard/utilidades/', include('apps.utilidades.urls', namespace="utilidades")),
] #+ static(settings.STATIC_ROOT, document_root = settings.STATIC_ROOT)
